# Environment variables may override defaults set below:
# Use Intel compilers (mpiicpc, icpc) by default, alternatively use GNU
# compilers (mpicxx, g++).
USE_GCC ?= 0
USE_MPI ?= 1
USE_OMP ?= 1

ifeq ($(USE_GCC),1)

# GCC
ifeq ($(USE_MPI),1)
  CXX = mpicxx
else
  CXX = g++
endif
ifeq ($(USE_OMP),1)
  FLAGS = -fopenmp
endif

else

# Intel Compiler
ifeq ($(USE_MPI),1)
  CXX = mpiicpc
else
  CXX = icpc
endif
ifeq ($(USE_OMP),1)
  FLAGS = -qopenmp
endif

endif

ifeq ($(USE_MPI),1)
  FLAGS += -DUSE_MPI
endif
ifeq ($(USE_OMP),1)
  FLAGS += -DUSE_OMP
endif

PINCHECK_HOME = $(shell pwd)

# pincheck is a header-only library
FLAGS += -I$(PINCHECK_HOME)/include

pincheck: example/pincheck.cc include/pincheck.hpp include/pincheck_implementation.hpp
	$(CXX) -Wall $(FLAGS) -g -o $@ $<

.PHONY: clean
clean:
	rm -f pincheck

