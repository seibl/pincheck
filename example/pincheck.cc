#include <string>
#include <iostream>

#ifdef USE_MPI
#include <mpi.h>
#endif

// Include pincheck.hpp after mpi.h to enable MPI auto detection
#include <pincheck.hpp>

int main(int argc, char ** argv) {
#ifdef USE_MPI
    MPI_Init(&argc, &argv);
#endif

    // Invoke 'pincheck::pincheck()' after MPI_Init() to
    // obtain a string with pinning information on rank 0,
    // ranks >0 get an empty string:
    const std::string pinning_info = pincheck::pincheck();
    std::cout << pinning_info;

#ifdef USE_MPI
    MPI_Finalize();
#endif
    return 0;
}

