// pincheck implementation file
// (do not include this file directly into user code,
// please include only 'pincheck.hpp'.

#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <unistd.h>
#include <limits.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/syscall.h>
#include <sched.h>
#include <time.h>
#include <pthread.h>

// add C++11 stuff if possible
#if __cplusplus > 199711L
#include <thread>
#include <chrono>
//#warning "enabling C++11 features"
#endif

#ifdef PINCHK_USE_MPI
#include <mpi.h>
#endif
#ifdef PINCHK_USE_OMP
#include <omp.h>
#endif

namespace pincheck {

#ifdef PINCHK_USE_MPI
const int PINCHK_HOST_NAME_MAX=MPI_MAX_PROCESSOR_NAME;
#else
const int PINCHK_HOST_NAME_MAX=HOST_NAME_MAX;
#endif

#ifndef PINCHK_USE_OMP
int omp_get_max_threads() { return 1; }
int omp_get_thread_num() { return 0; }
#endif


// maximum logical processor id up to which we check
const int PINCHK_MAX_PROC_ID=1024;
// buffer sufficiently large to hold a single output line
const int PINCHK_BUFSIZE=4096;


int _get_number_of_online_processors() {
    return sysconf(_SC_NPROCESSORS_ONLN);
}


void _get_thread_affinity(cpu_set_t &cpu_set) {
    CPU_ZERO(&cpu_set);
    pid_t tid = syscall(SYS_gettid);
    sched_getaffinity(tid, sizeof(cpu_set_t), &cpu_set);
}


void _get_processor_ids(cpu_set_t &cpu_set, std::vector<int> &proc_set) {
    for (int i=0; i<PINCHK_MAX_PROC_ID; ++i) {
        if (CPU_ISSET(i, &cpu_set)) {
            proc_set.push_back(i);
        }
    }
}


std::string pincheck() {
    int n_mpi;
    int i_mpi;
#ifdef PINCHK_USE_MPI
    MPI_Comm_size(MPI_COMM_WORLD, &n_mpi);
    MPI_Comm_rank(MPI_COMM_WORLD, &i_mpi);
#else
    n_mpi=1;
    i_mpi=0;
#endif
    const int n_omp = omp_get_max_threads();
    const int n_cpu = _get_number_of_online_processors();
    const int n_elem_max_local = n_omp * n_cpu;
    std::vector<int> local_binding_information(n_elem_max_local, -1);

    #pragma omp parallel default(shared)
    {
        int i_omp = omp_get_thread_num();
        cpu_set_t cpu_set;
        _get_thread_affinity(cpu_set);
        std::vector<int> proc_set;
        _get_processor_ids(cpu_set, proc_set);

        #pragma omp critical
        {
            int off = i_omp * n_cpu;
            for (unsigned int i=0; i<proc_set.size(); ++i) {
                local_binding_information[off + i] = proc_set[i];
            }
        }
    }

    // gather binding information to master process
    const int n_elem_max_global = n_mpi * n_elem_max_local;
#ifdef PINCHK_USE_MPI
    std::vector<int> global_binding_information(n_elem_max_global, -1);
    MPI_Gather(local_binding_information.data(), n_elem_max_local, MPI_INT,
               global_binding_information.data(), n_elem_max_local, MPI_INT,
               0, MPI_COMM_WORLD);
#else
    std::vector<int> global_binding_information = local_binding_information;
#endif

    // gather hostnames to master process
    std::vector<char> host(PINCHK_HOST_NAME_MAX, '\0');
#ifdef PINCHK_USE_MPI
    int name_len;
    MPI_Get_processor_name(host.data(), &name_len);
    std::vector<char> hostnames(n_mpi*PINCHK_HOST_NAME_MAX, '\0');
    MPI_Gather(host.data(), PINCHK_HOST_NAME_MAX, MPI_CHAR,
               hostnames.data(), PINCHK_HOST_NAME_MAX, MPI_CHAR,
               0, MPI_COMM_WORLD);
#else
    gethostname(host.data(), PINCHK_HOST_NAME_MAX);
    std::vector<char> hostnames = host;
#endif

    std::stringstream buf;
    const int hwdth = 24;
    const int iwdth = 10;

    if (i_mpi == 0) {
        buf << std::setw(hwdth) << "hostname" << std::setw(iwdth) << "task" << std::setw(iwdth) << "thread" << std::setw(iwdth) << "cpus" << std::endl;
        for (int i=0; i<n_mpi; ++i) {
            int oi = i * n_elem_max_local;
            for (int j=0; j<n_omp; j++) {
                int oj = j * n_cpu;
                int oh = i * PINCHK_HOST_NAME_MAX;
                std::string hostname_trunc(std::string(hostnames.data()+oh).substr(0,hwdth));
                buf << std::setw(hwdth) << hostname_trunc << std::setw(iwdth) << i << std::setw(iwdth) << j << "     ";
                for (int k=0; k<n_cpu; ++k) {
                    int v = global_binding_information[oi + oj + k];
                    if (v < 0) {
                        break;
                    } else {
                        buf << " " << v;
                    }
                }
                buf << std::endl;
            }
        }
    }

#if __cplusplus > 199711L
    if(const char* env_p = getenv("PINCHECK_SLEEP")) {
        const int sleep_s = std::stoi(env_p);
        std::this_thread::sleep_for(std::chrono::seconds(sleep_s));
    }
#endif

    return buf.str();
}

} // end namespace pincheck
