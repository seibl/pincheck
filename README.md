# pincheck

## Introduction

'pincheck' is a C++ header-only library (and optionally, a standalone
executable) to check the cpu binding (pinning) in MPI and OpenMP parallel
codes at runtime. A typical use case is to check the pinning in the context
of an actual batch job on a HPC system in order to verify the user's batch
script and the batch system configuration.

'pincheck' returns the cpu binding based on the genuine information it gets
from the operating system via system calls (i.e. independent of the compiler,
OpenMP runtime, or MPI library) for each MPI process and OpenMP thread in a
tabular form. MPI and OpenMP can be enabled or disabled at compile time
independently.

## Usage

The program 'example/pincheck.cc' shows how to use the header-only library
from any C++ program. It can be used in any C++ code in the same fashion.
A library for linking to C and Fortran programs is currently under development.
Alternatively, after you have compiled the example program, run `pincheck` as
you would do with any OpenMP/MPI program and inspect the output.

## Compilation

On MPCDF systems, load a compiler and MPI (e.g. Intel) and run `make` to
compile:

```bash
module purge
module load intel/19.1.3 impi/2019.9
make
```

Make sure to load the very same combination of the compiler and MPI when running
the `pincheck` executable.

The Make variable 'USE\_GCC=1' can be used to switch to GCC compilers.  Adapt the
Makefile in case you want to compile using other compilers or MPI wrappers.  The
following compilation modes are supported via Make variables:

```bash
make USE_GCC=1             # use `mpic++` and `g++` as the compilers
make USE_OMP=1 USE_MPI=1   # default
make USE_OMP=1 USE_MPI=0
make USE_OMP=0 USE_MPI=1
make USE_OMP=0 USE_MPI=0
```

## Examples

### Hybrid run on two nodes only using process binding and no OpenMP binding

#### Slurm batch script

```bash
#!/bin/bash
#SBATCH -D ./
#SBATCH -J pincheck
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=2
#SBATCH --cpus-per-task=20
#SBATCH --mail-type=none
#SBATCH --time=0:01:59
module purge
module load intel/19.1.3 impi/2019.9
export OMP_NUM_THREADS=${SLURM_CPUS_PER_TASK}
srun ./pincheck
```

#### pincheck output

```text
                hostname      task    thread      cpus
                  co6633         0         0      0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19
                  co6633         0         1      0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19
                  co6633         0         2      0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19
                  co6633         0         3      0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19
                  co6633         0         4      0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19
                  co6633         0         5      0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19
                  co6633         0         6      0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19
                  co6633         0         7      0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19
                  co6633         0         8      0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19
                  co6633         0         9      0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19
                  co6633         0        10      0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19
                  co6633         0        11      0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19
                  co6633         0        12      0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19
                  co6633         0        13      0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19
                  co6633         0        14      0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19
                  co6633         0        15      0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19
                  co6633         0        16      0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19
                  co6633         0        17      0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19
                  co6633         0        18      0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19
                  co6633         0        19      0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19
                  co6633         1         0      20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39
                  co6633         1         1      20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39
                  co6633         1         2      20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39
                  co6633         1         3      20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39
                  co6633         1         4      20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39
                  co6633         1         5      20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39
                  co6633         1         6      20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39
                  co6633         1         7      20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39
                  co6633         1         8      20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39
                  co6633         1         9      20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39
                  co6633         1        10      20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39
                  co6633         1        11      20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39
                  co6633         1        12      20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39
                  co6633         1        13      20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39
                  co6633         1        14      20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39
                  co6633         1        15      20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39
                  co6633         1        16      20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39
                  co6633         1        17      20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39
                  co6633         1        18      20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39
                  co6633         1        19      20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39
                  co6635         2         0      0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19
                  co6635         2         1      0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19
                  co6635         2         2      0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19
                  co6635         2         3      0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19
                  co6635         2         4      0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19
                  co6635         2         5      0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19
                  co6635         2         6      0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19
                  co6635         2         7      0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19
                  co6635         2         8      0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19
                  co6635         2         9      0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19
                  co6635         2        10      0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19
                  co6635         2        11      0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19
                  co6635         2        12      0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19
                  co6635         2        13      0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19
                  co6635         2        14      0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19
                  co6635         2        15      0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19
                  co6635         2        16      0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19
                  co6635         2        17      0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19
                  co6635         2        18      0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19
                  co6635         2        19      0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19
                  co6635         3         0      20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39
                  co6635         3         1      20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39
                  co6635         3         2      20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39
                  co6635         3         3      20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39
                  co6635         3         4      20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39
                  co6635         3         5      20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39
                  co6635         3         6      20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39
                  co6635         3         7      20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39
                  co6635         3         8      20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39
                  co6635         3         9      20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39
                  co6635         3        10      20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39
                  co6635         3        11      20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39
                  co6635         3        12      20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39
                  co6635         3        13      20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39
                  co6635         3        14      20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39
                  co6635         3        15      20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39
                  co6635         3        16      20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39
                  co6635         3        17      20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39
                  co6635         3        18      20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39
                  co6635         3        19      20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39
```

### Hybrid run on two nodes using hyperthreading and per-hardware-thread pinning:

#### Slurm batch script

```bash
#!/bin/bash
#SBATCH -D ./
#SBATCH -J pincheck
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=2
#SBATCH --ntasks-per-core=2
#SBATCH --cpus-per-task=40
#SBATCH --mail-type=none
#SBATCH --time=0:01:59
module purge
module load intel/19.1.3 impi/2019.9
export SLURM_HINT=multithread
export OMP_NUM_THREADS=${SLURM_CPUS_PER_TASK}
export OMP_PLACES=threads
srun ./pincheck
```

#### pincheck output

```text
                hostname      task    thread      cpus
                  co6633         0         0      0
                  co6633         0         1      40
                  co6633         0         2      1
                  co6633         0         3      41
                  co6633         0         4      2
                  co6633         0         5      42
                  co6633         0         6      3
                  co6633         0         7      43
                  co6633         0         8      4
                  co6633         0         9      44
                  co6633         0        10      5
                  co6633         0        11      45
                  co6633         0        12      6
                  co6633         0        13      46
                  co6633         0        14      7
                  co6633         0        15      47
                  co6633         0        16      8
                  co6633         0        17      48
                  co6633         0        18      9
                  co6633         0        19      49
                  co6633         0        20      10
                  co6633         0        21      50
                  co6633         0        22      11
                  co6633         0        23      51
                  co6633         0        24      12
                  co6633         0        25      52
                  co6633         0        26      13
                  co6633         0        27      53
                  co6633         0        28      14
                  co6633         0        29      54
                  co6633         0        30      15
                  co6633         0        31      55
                  co6633         0        32      16
                  co6633         0        33      56
                  co6633         0        34      17
                  co6633         0        35      57
                  co6633         0        36      18
                  co6633         0        37      58
                  co6633         0        38      19
                  co6633         0        39      59
                  co6633         1         0      20
                  co6633         1         1      60
                  co6633         1         2      21
                  co6633         1         3      61
                  co6633         1         4      22
                  co6633         1         5      62
                  co6633         1         6      23
                  co6633         1         7      63
                  co6633         1         8      24
                  co6633         1         9      64
                  co6633         1        10      25
                  co6633         1        11      65
                  co6633         1        12      26
                  co6633         1        13      66
                  co6633         1        14      27
                  co6633         1        15      67
                  co6633         1        16      28
                  co6633         1        17      68
                  co6633         1        18      29
                  co6633         1        19      69
                  co6633         1        20      30
                  co6633         1        21      70
                  co6633         1        22      31
                  co6633         1        23      71
                  co6633         1        24      32
                  co6633         1        25      72
                  co6633         1        26      33
                  co6633         1        27      73
                  co6633         1        28      34
                  co6633         1        29      74
                  co6633         1        30      35
                  co6633         1        31      75
                  co6633         1        32      36
                  co6633         1        33      76
                  co6633         1        34      37
                  co6633         1        35      77
                  co6633         1        36      38
                  co6633         1        37      78
                  co6633         1        38      39
                  co6633         1        39      79
                  co6635         2         0      0
                  co6635         2         1      40
                  co6635         2         2      1
                  co6635         2         3      41
                  co6635         2         4      2
                  co6635         2         5      42
                  co6635         2         6      3
                  co6635         2         7      43
                  co6635         2         8      4
                  co6635         2         9      44
                  co6635         2        10      5
                  co6635         2        11      45
                  co6635         2        12      6
                  co6635         2        13      46
                  co6635         2        14      7
                  co6635         2        15      47
                  co6635         2        16      8
                  co6635         2        17      48
                  co6635         2        18      9
                  co6635         2        19      49
                  co6635         2        20      10
                  co6635         2        21      50
                  co6635         2        22      11
                  co6635         2        23      51
                  co6635         2        24      12
                  co6635         2        25      52
                  co6635         2        26      13
                  co6635         2        27      53
                  co6635         2        28      14
                  co6635         2        29      54
                  co6635         2        30      15
                  co6635         2        31      55
                  co6635         2        32      16
                  co6635         2        33      56
                  co6635         2        34      17
                  co6635         2        35      57
                  co6635         2        36      18
                  co6635         2        37      58
                  co6635         2        38      19
                  co6635         2        39      59
                  co6635         3         0      20
                  co6635         3         1      60
                  co6635         3         2      21
                  co6635         3         3      61
                  co6635         3         4      22
                  co6635         3         5      62
                  co6635         3         6      23
                  co6635         3         7      63
                  co6635         3         8      24
                  co6635         3         9      64
                  co6635         3        10      25
                  co6635         3        11      65
                  co6635         3        12      26
                  co6635         3        13      66
                  co6635         3        14      27
                  co6635         3        15      67
                  co6635         3        16      28
                  co6635         3        17      68
                  co6635         3        18      29
                  co6635         3        19      69
                  co6635         3        20      30
                  co6635         3        21      70
                  co6635         3        22      31
                  co6635         3        23      71
                  co6635         3        24      32
                  co6635         3        25      72
                  co6635         3        26      33
                  co6635         3        27      73
                  co6635         3        28      34
                  co6635         3        29      74
                  co6635         3        30      35
                  co6635         3        31      75
                  co6635         3        32      36
                  co6635         3        33      76
                  co6635         3        34      37
                  co6635         3        35      77
                  co6635         3        36      38
                  co6635         3        37      78
                  co6635         3        38      39
                  co6635         3        39      79
```
